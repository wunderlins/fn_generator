#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <math.h>
#include "driver/ledc.h"
#include "esp_err.h"

#include "dac-cosine.h"
#include "display.h"
#include "main.h"

// I2C oled display library
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);

// FreeRTOS Task handle for the main Task
TaskHandle_t TaskMainHandle;
TaskHandle_t TaskSinHandle;

// global settings
int last_pot = 0; // last pot position, for debouncing
const int grace_pot = 8; // debounce threshold
int freq = 10000; // initial wave frequency
uint8_t cycle = 127; // initial dutiy cycle (0-255: 50%)

// sine wave target frequency
extern int frequency_step;

// global variables avialable to interrupt handlers therefore volatile
volatile input_mode_t mode; // might be pwm or duty
volatile bool screenrefresh = false; // if true, force a screen refresh on nect main task cycle
volatile unsigned long lastDebounceTime = 0; // debounce time for button
volatile const unsigned long debounceDelay = 300; // debouncy timeout for button

void setSquareWave(int freq, int cycle) {
	// set PWM options. The pot introduces som input jitter. We have to stop
	// the timer here and restart it. Therefore be careful to only do it 
	// when needed (i.e. on relevant input changes, hence the debounce)
	ledcDetachPin(SIG_PIN);
	delay(5);
	ledcSetup(PWM_CHAN, freq, 8);
	delay(5);
	ledcAttachPin(SIG_PIN, PWM_CHAN);
	delay(5);
	ledcWrite(PWM_CHAN, cycle);
}

void sineTask() {
	dac_cosine_enable(DAC_CHANNEL_1);
	//dac_cosine_enable(DAC_CHANNEL_2);

	dac_output_enable(DAC_CHANNEL_1);
	//dac_output_enable(DAC_CHANNEL_2);

	xTaskCreatePinnedToCore(
	                  dactask,   /* Task function. */
	                  "TaskSine",     /* name of task. */
	                  1024*3,       /* Stack size of task */
	                  NULL,        /* parameter of the task */
	                  1,           /* priority of the task */
	                  &TaskSinHandle,      /* Task handle to keep track of created task */
	                  0);          /* pin task to core 0 */                  
	delay(150); 
}

void TaskMain(void * pvParameters) {

	for(;;){
		// supress jitter from potentiometer
		int f  = analogRead(POT_PIN);
		
		// check if pot moved enoug to make a freq or duty update
		if ((last_pot + grace_pot < f || last_pot - grace_pot > f) || // pot has moved
					screenrefresh // mode might have been switched, re-read pot
				) {
			
			Serial.print("Pot Changed, last: ");
			Serial.print(last_pot);
			Serial.print(", new: ");
			Serial.println(f);
			last_pot = f;

			if (mode.pwm)
				freq = map(f, 4096, 0, 300000, 1);
			if (mode.duty) // allow 10% - 90%
				cycle = map(f, 4096, 0, 228, 25);

			//setSquareWave(freq, cycle);

			// sine frequency
			freq = map(f, 4096, 0, 2000, 0);
			frequency_step = freq;

			// restart the sine task
			//vTaskDelete(TaskSinHandle);
			//sineTask();

			// value updated, force a screen refresh.
			screenrefresh = true;
		}

		// update display
		if (screenrefresh) {
			screenrefresh = false;
			drawScreen();
		}

		// sleep for a bit, this helps reduce unneccesary screen refreshes and
		// helps to keep the pwm output stable.
		delay(200);
	}
}

void changeModeFalling() {
	
	// debounce button
	if ((millis() - lastDebounceTime) < debounceDelay)
		return;

	// reverse input mode
	if (mode.pwm) {
		mode.pwm = false;
		mode.duty = true;
	} else {
		mode.pwm = true;
		mode.duty = false;
	}

	/*
	Serial.print("Button FALLING, pwm: ");
	Serial.print(mode.pwm);
	Serial.print(", duty: ");
	Serial.print(mode.duty);
	Serial.print("\n");
	*/

	// force a screen redraw, mode needs to be changed on the screen
	Serial.println("Mode Changed ...");
	screenrefresh = true;

	// rember this trigger so we can properly debounce the button
	lastDebounceTime = millis();
}

// the setup function runs once when you press reset or power the board
void setup() {
	// serial connection, we have it, we use it :)
	Serial.begin(9600);

	// initialize reversed, upon startup the button ISR will be triggered
	mode.pwm = false;
	mode.duty = true;

	// initialize some pins such as the pot, led and Button
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(POT_PIN, INPUT);
	pinMode(BUT_PIN, INPUT_PULLUP);

	//ledcSetup(PWM_CHAN, 1200, 16);
	//ledcAttachPin(SIG_PIN, PWM_CHAN);

	// initialize OLED Pins
	pinMode(OLED_RST, OUTPUT);
	digitalWrite(OLED_RST, LOW);
	delay(20);
	digitalWrite(OLED_RST, HIGH);

	// initialize oled SPI Connection
	Wire.begin(OLED_SDA, OLED_SCL);
	// Address 0x3C for 128x64
	if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3c, false, false)) { 
		Serial.println(F("SSD1306 allocation failed"));
		for(;;); // Don't proceed, loop forever
	}

	// setup text on display
	display.clearDisplay();
	display.setTextColor(WHITE);
	display.setTextSize(2);
	display.setCursor(0,0);

	//create a task that will be executed in the Task1code() function, with priority 1 and executed on core 0
	xTaskCreatePinnedToCore(
	                  TaskMain,   /* Task function. */
	                  "TaskMain",     /* name of task. */
	                  20000,       /* Stack size of task */
	                  NULL,        /* parameter of the task */
	                  1,           /* priority of the task */
	                  &TaskMainHandle,      /* Task handle to keep track of created task */
	                  1);          /* pin task to core 0 */                  
	delay(150); 

	sineTask();

	// attach interrupt to button
	attachInterrupt(digitalPinToInterrupt(BUT_PIN), changeModeFalling, FALLING);
}

void loop() {;}
