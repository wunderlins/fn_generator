#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "main.h"

// global variables from main.cpp
extern Adafruit_SSD1306 display;
extern int freq;
extern uint8_t cycle; // duty cycle
extern input_mode_t mode;

/**
 * Display the frequency
 */
void displayLine1() {
	display.print("   ");
	// padding
	if (freq < 10 || freq < 10000)   display.print(" ");
	if (freq < 100 || freq < 100000) display.print(" ");
	
	// kilo / herz
	int fr = freq;
	if (freq > 1000)   fr = fr / 1000;
	display.print(fr);
	if (freq > 1000)   display.print(" KHz");
	else               display.print("  Hz");
	display.print("\n");
}

/**
 * Display mode symbol
 */
void displayRectWave(int pos_x, int pos_y, int height=8, int invert=false) {
	// drawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);
	
	int fgcolor = WHITE;
	int bgcolor = 0;

	int start_x = pos_x+1;
	int start_y = pos_y+height - 2;
	int width = 5;

	if (invert) {
		fgcolor = 0;
		bgcolor = WHITE;
		display.fillRect(pos_x, pos_y, width*5+1+2, height, bgcolor);
	}

	height = height - 3;

	//int height  = 8;
	display.drawFastHLine(start_x + 0, start_y, (width+1), fgcolor);
	display.drawFastVLine(start_x + width, start_y-height, height, fgcolor);
	display.drawFastHLine(start_x + width, start_y-height, (width+1), fgcolor);
	display.drawFastVLine(start_x + (width*2), start_y-height, height, fgcolor);

	display.drawFastHLine(start_x + (width*2), start_y, (width+1), fgcolor);
	display.drawFastVLine(start_x + (width*3), start_y-height, height, fgcolor);
	display.drawFastHLine(start_x + (width*3), start_y-height, (width+1), fgcolor);
	display.drawFastVLine(start_x + (width*4), start_y-height, height, fgcolor);
	
	display.drawFastHLine(start_x + (width*4), start_y, (width+1), fgcolor);
}

/**
 * Display duty cicle line
 */
void displayDutyCycle(int pos_x, int pos_y, int height=8, int invert=false) {
	int fgcolor = WHITE;
	int bgcolor = 0;

	if (invert) {
		fgcolor = 0;
		bgcolor = WHITE;
		display.fillRect(pos_x, pos_y, 28, height, bgcolor);
	}

	int top = pos_y;
	int base = top + 2 + 5 + 1 ;
	display.fillRect(pos_x+5, base-1, 14, 3, fgcolor);
	display.fillTriangle(pos_x+8, base - 4, 
	                     pos_x+8, base + 4, 
											 pos_x+2, base , fgcolor);
	display.fillTriangle(pos_x+19, base - 4, 
	                     pos_x+19, base + 4, 
											 pos_x+19+6, base , fgcolor);

	int dcp = (cycle * 100 / 255 + 1);
	int dcph = (dcp / 10) + 48;
	int dcpl = (dcp % 10) + 48;

	display.drawChar(48, top, dcph, WHITE, 0, 2);
	display.drawChar(60, top, dcpl, WHITE, 0, 2);
	display.drawChar(72, top, ' ', WHITE, 0, 2);
	display.drawChar(84, top, '%', WHITE, 0, 2);
}

/**
 * Update the whole screen
 */
void drawScreen() {
	Serial.println("Updating screen ... ");
	// update dispaly with new value
	display.clearDisplay();
	display.setCursor(0, 0);
	displayLine1();
	displayRectWave(0, 0, 14, mode.pwm);
	display.drawFastHLine(0, 18, SCREEN_WIDTH, WHITE);
	displayDutyCycle(0, 22, 16, mode.duty);
	display.display();
}