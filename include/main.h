#define BUT_PIN 0
#define POT_PIN 36
#define SIG_PIN 25
#define PWM_CHAN 8

// OLED Settings
#define OLED_SDA 4
#define OLED_SCL 15 
#define OLED_RST 16
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

/**
 * Keep track of the input state
 * 
 * `pwm` is the mode where we accept new frequency settings
 * `duty` accepts a new value for the duty cycle
 */
typedef struct {
	bool pwm = true;
	bool duty = false;
} input_mode_t;